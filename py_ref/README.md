# share/py_ref #

Reference files from [refftta][1]. Each folder corresponds to one output type.
The files follow the following naming convention: 'ref_name_Nexp' where 'name'
is the name of the output and 'Nexp' is the FFT size as a power of 2
(N = 2^Nexp).

[1]: https://gitlab.com/fft-on-tta/refftta

[//]: # (tree)
## Directory tree ##
~~~~
/home/kubouch/git/thesis/share/py_ref
├── ag
├── cadd1
├── inp
├── out
├── tfg_k
├── tfg_tf
└── README.md
~~~~
