# share/tce #

Files from [ttf-in-tce][1] are stored here. This includes memory dumps and diff
files.

[1]: https://gitlab.com/fft-on-tta/fft-in-tce

[//]: # (tree)
## Directory tree ##
~~~~
/home/kubouch/git/thesis/share/tce
├── asm_dump   // Used for both diffs and dumps
└── README.md
~~~~
