# share #

Shared files between [refftta][1] and [fft-in-tce][2].

[1]: https://gitlab.com/fft-on-tta/refftta
[2]: https://gitlab.com/fft-on-tta/fft-in-tce

[//]: # (tree)
## Directory tree ##
~~~~
/home/kubouch/git/thesis/share
├── py_ref     // Reference files (from [refftta][1] by default)
├── tce        // Memory dump and diff files from [fft-in-tce][2]
├── tf_lut     // C header with twiddle factors which serves as a LUT
├── fpconv.py  // Converter to/from fixed point representation
├── README.md
~~~~
